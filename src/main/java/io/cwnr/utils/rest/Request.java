package io.cwnr.utils.rest;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.databind.node.ObjectNode;

public record Request(
        String url,
        Method method,
        ObjectNode body,
        HashMap<String, List<String>> headers,
        HashMap<String, String> params
) {
    static public Request empty() {
        return new Request("", Method.GET, null, new HashMap<String, List<String>>(), new HashMap<String,String>());
    }
    
    public Request setUrl(String url) {
        return new Request(
            url, 
            this.method, 
            this.body,
            this.headers,
            this.params
        );
    }

    public Request setMethod(Method method) {
        return new Request(
            this.url,
            method,
            this.body,
            this.headers,
            this.params
        );
    }

    public Request setBody(ObjectNode body) {
        return new Request(
            this.url, 
            this.method, 
            body,
            this.headers,
            this.params
        );
    }

    public Request addHeader(String name, String value) {
        HashMap<String, List<String>> headers = this.headers;
        List<String> headerToChange = new LinkedList<String>();
        
        if (headers.containsKey(name)) {
            headerToChange = headers.get(name);
        }

        headerToChange.add(value);
        headers.put(name, headerToChange);
        return new Request(
            this.url,
            this.method,
            this.body,
            headers,
            this.params
        );
    }

    public Request addParam(String name, String value) {
        HashMap<String, String> params = this.params;
        params.put(name, value);
        return new Request(
            this.url,
            this.method,
            this.body,
            this.headers,
            params
        );
    }

    public Optional<Response> call() throws IOException {
        return RestClient.call(this);
    }
}
