package io.cwnr.utils.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

public class RestClient {
    static ObjectMapper objectMapper = new ObjectMapper();

    static public Optional<Response> call(Request input) throws IOException {
        Response result = null;
        try {
            HttpRequest.Builder builder = HttpRequest.newBuilder();
            
            // Add URI
            String url = input.url();

            if (input.params().size() > 0) {
                // We have some query params, let's encode and add them to url
                Boolean firstParam = true;
                HashMap<String, String> params = input.params();
                for (String paramName : params.keySet()) {
                    String paramValue = URLEncoder.encode(params.get(paramName), StandardCharsets.UTF_8.toString());
                    url = firstParam ? url + '?' : url + '&';
                    firstParam = false;
                    url = url + paramName + '=' + paramValue;
                }
            }

            URI uri = new URI(url);
            builder = builder.uri(uri);

            // Add headers
            Map<String, List<String>> headers = input.headers();
            for (String name : headers.keySet()) {
                for (String value : headers.get(name)) {
                    builder = builder.setHeader(name, value);
                }
            }
            
            // Create body
            HttpRequest.BodyPublisher body = HttpRequest.BodyPublishers.noBody();
            if (input.body() != null) {
                body = HttpRequest.BodyPublishers.ofString(input.body().toString());
            }
            
            // Build request
            switch (input.method().name()) {
                case "GET":
                    builder = builder.GET();
                    break;
                case "DELETE":
                    builder = builder.DELETE();
                    break;
                case "POST":
                    builder = builder.POST(body);
                    break;
                case "PUT":
                    builder = builder.PUT(body);
                    break;
            }

            HttpRequest request = builder.build();
            HttpResponse<String> response = HttpClient.newBuilder().
                followRedirects(HttpClient.Redirect.ALWAYS)
                .connectTimeout(Duration.ofSeconds(30L))
                .build()
                .send(request, HttpResponse.BodyHandlers.ofString());
            
            result = new Response(
                response.statusCode(),
                objectMapper.readTree(response.body()),
                response.headers().map()
            );
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Optional.ofNullable(result);
    }
}

