package io.cwnr.utils.rest;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public record Response(Integer code, JsonNode body, Map<String, List<String>> headers) {
    static ObjectMapper objectMapper = new ObjectMapper();

    public <T> T as(Class<T> clazz) throws JsonProcessingException {
        return objectMapper.treeToValue(body, clazz);
    }
}