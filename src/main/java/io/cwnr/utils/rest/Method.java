package io.cwnr.utils.rest;

public enum Method {
    GET,
    POST,
    PUT,
    DELETE
}
