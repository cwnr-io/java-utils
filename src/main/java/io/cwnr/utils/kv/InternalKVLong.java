package io.cwnr.utils.kv;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class InternalKVLong implements KVLong {
    private final ConcurrentHashMap<String, Long> hashmap;

    public InternalKVLong() {
        this.hashmap = new ConcurrentHashMap<String, Long>();
    };

    public String toString() {
        return "InternalKVLong<> [" + size() + " elements]";
    };

    public void clear() {
        hashmap.clear();
    };

    public long size() {
        return hashmap.mappingCount();
    };

    public List<String> keys() {
        List<String> keys = new LinkedList<String>();
        for (String key : hashmap.keySet()) {
            keys.add(key);
        }
        return keys;
    };

    public List<Long> values() {
        List<Long> values = new LinkedList<Long>();
        for (Long value : hashmap.values()) {
            values.add(value);
        }
        return values;
    };

    public Optional<Long> get(String key) {
        return Optional.ofNullable(hashmap.get(key));
    };

    public Long getOrDefault(String key, Long defaultValue) {
        return get(key).orElse(defaultValue);
    };

    public Optional<Long> set(String key, Long value) {
        Optional<Long> previousValue = get(key);
        hashmap.put(key, value);
        return previousValue;
    };

    public Optional<Long> del(String key) {
        Optional<Long> previousValue = get(key);
        hashmap.remove(key);
        return previousValue;
    };

    public boolean exists(String key) {
        return hashmap.containsKey(key);
    }

    public Long incr(String key) {
        return hashmap.compute(key, (k, v) -> Optional.ofNullable(v).map(x -> x+1).orElse(1L));
    };
}
