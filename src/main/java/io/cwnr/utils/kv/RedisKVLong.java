package io.cwnr.utils.kv;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisKVLong implements KVLong {

    private final JedisPool jedisPool;
    private final String hashMapName;

    public RedisKVLong(JedisPool jedisPool, String hashMapName) {
        this.jedisPool = jedisPool;
        this.hashMapName = hashMapName;
    };

    public void clear() {
        try (Jedis redis = jedisPool.getResource()) {
            redis.del(hashMapName);
        }
    };

    public long size() {
        try (Jedis redis = jedisPool.getResource()) {
            return redis.hlen(hashMapName);
        }
    };

    public List<String> keys() {
        List<String> result = new LinkedList<String>();
        try (Jedis redis = jedisPool.getResource()) {
            for (String key : redis.hkeys(hashMapName)) result.add(key);
        }
        return result;
    };

    public List<Long> values() {
        List<Long> result = new LinkedList<Long>();
        try (Jedis redis = jedisPool.getResource()) {
            for (String value : redis.hvals(hashMapName)) result.add(Long.decode(value));
        }
        return result;
    };

    public Optional<Long> get(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            return Optional.ofNullable(redis.hget(hashMapName, key)).map(v -> Long.decode(v));
        }
    };

    public Long getOrDefault(String key, Long defaultValue) {
        return get(key).orElse(defaultValue);
    };

    public Optional<Long> set(String key, Long value) {
        try (Jedis redis = jedisPool.getResource()) {
            Optional<Long> previousValue = get(key);
            redis.hset(hashMapName, key, value.toString());
            return previousValue;
        }
    };

    public Optional<Long> del(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            Optional<Long> previousValue = get(key);
            redis.hdel(hashMapName, key);
            return previousValue;
        }
    };

    public boolean exists(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            return redis.hexists(hashMapName, key);
        }
    };
    
    public Long incr(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            return redis.hincrBy(hashMapName, key, 1L);
        }
    };
}
