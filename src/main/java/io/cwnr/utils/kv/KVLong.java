package io.cwnr.utils.kv;

import java.util.List;
import java.util.Optional;

public interface KVLong {
    public void clear();
    public long size();

    public List<String> keys();
    public List<Long> values();

    public Optional<Long> get(String key);
    public Long getOrDefault(String key, Long defaultValue);

    public Optional<Long> set(String key, Long value);

    public Optional<Long> del(String key);

    public boolean exists(String key);
    
    public Long incr(String key);
}
