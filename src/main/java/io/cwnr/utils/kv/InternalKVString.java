package io.cwnr.utils.kv;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

public class InternalKVString implements KVString {
    private final ConcurrentHashMap<String, String> hashmap;

    public InternalKVString() {
        this.hashmap = new ConcurrentHashMap<String, String>();
    };

    public String toString() {
        return "InternalKVString<> [" + size() + " elements]";
    };

    public void clear() {
        hashmap.clear();
    };

    public long size() {
        return hashmap.mappingCount();
    };

    public List<String> keys() {
        List<String> keys = new LinkedList<String>();
        for (String key : hashmap.keySet()) {
            keys.add(key);
        }
        return keys;
    };

    public List<String> values() {
        List<String> values = new LinkedList<String>();
        for (String value : hashmap.values()) {
            values.add(value);
        }
        return values;
    };

    public Optional<String> get(String key) {
        return Optional.ofNullable(hashmap.get(key));
    };

    public String getOrDefault(String key, String defaultValue) {
        return get(key).orElse(defaultValue);
    };

    public Optional<String> set(String key, String value) {
        Optional<String> previousValue = get(key);
        hashmap.put(key, value);
        return previousValue;
    };

    public Optional<String> del(String key) {
        Optional<String> previousValue = get(key);
        hashmap.remove(key);
        return previousValue;
    };

    public boolean exists(String key) {
        return hashmap.containsKey(key);
    }
}
