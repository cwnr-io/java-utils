package io.cwnr.utils.kv;

import java.util.List;
import java.util.Optional;

public interface KVString {
    public void clear();
    public long size();

    public List<String> keys();
    public List<String> values();

    public Optional<String> get(String key);
    public String getOrDefault(String key, String defaultValue);

    public Optional<String> set(String key, String value);
    
    public Optional<String> del(String key);

    public boolean exists(String key);
}
