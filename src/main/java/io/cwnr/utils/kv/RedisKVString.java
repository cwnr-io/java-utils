package io.cwnr.utils.kv;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisKVString implements KVString {

    private final JedisPool jedisPool;
    private final String hashMapName;

    public RedisKVString(JedisPool jedisPool, String hashMapName) {
        this.jedisPool = jedisPool;
        this.hashMapName = hashMapName;
    };

    public void clear() {
        try (Jedis redis = jedisPool.getResource()) {
            redis.del(hashMapName);
        }
    };

    public long size() {
        try (Jedis redis = jedisPool.getResource()) {
            return redis.hlen(hashMapName);
        }
    };

    public List<String> keys() {
        List<String> result = new LinkedList<String>();
        try (Jedis redis = jedisPool.getResource()) {
            for (String key : redis.hkeys(hashMapName)) result.add(key);
        }
        return result;
    };

    public List<String> values() {
        List<String> result = new LinkedList<String>();
        try (Jedis redis = jedisPool.getResource()) {
            for (String value : redis.hvals(hashMapName)) result.add(value);
        }
        return result;
    };

    public Optional<String> get(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            return Optional.ofNullable(redis.hget(hashMapName, key));
        }
    };

    public String getOrDefault(String key, String defaultValue) {
        return get(key).orElse(defaultValue);
    };

    public Optional<String> set(String key, String value) {
        try (Jedis redis = jedisPool.getResource()) {
            Optional<String> previousValue = get(key);
            redis.hset(hashMapName, key, value.toString());
            return previousValue;
        }
    };

    public Optional<String> del(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            Optional<String> previousValue = get(key);
            redis.hdel(hashMapName, key);
            return previousValue;
        }
    };

    public boolean exists(String key) {
        try (Jedis redis = jedisPool.getResource()) {
            return redis.hexists(hashMapName, key);
        }
    };
}
