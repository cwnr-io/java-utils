package io.cwnr.utils.db.config;

public record AuthParams(String jdbcUrl, String username, String password) {}
