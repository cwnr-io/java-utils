package io.cwnr.utils.db.config;

public record PoolParams(String name, int minPoolSize, int maxPoolSize) {};
