package io.cwnr.utils.db.fn;

import java.sql.SQLException;
import java.sql.Statement;

@FunctionalInterface
public interface StatementProcessor<P> {
    public P process(Statement stmt) throws SQLException;
}
