package io.cwnr.utils.db.fn;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface Binder<I> {
    void bind(PreparedStatement preparedStatement, I inputElement) throws SQLException;
}