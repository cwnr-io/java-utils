package io.cwnr.utils.db.fn;

import java.sql.ResultSet;
import java.sql.SQLException;

@FunctionalInterface
public interface Mapper<O> {
    O map(ResultSet resultSet) throws SQLException;
}