package io.cwnr.utils.db.fn;

import java.sql.PreparedStatement;
import java.sql.SQLException;

@FunctionalInterface
public interface PreparedStatementProcessor<P> {
    public P process(PreparedStatement stmt) throws SQLException;
}
