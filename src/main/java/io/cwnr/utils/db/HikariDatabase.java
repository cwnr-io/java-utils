package io.cwnr.utils.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Duration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.codahale.metrics.MetricRegistry;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.cwnr.utils.db.config.AuthParams;
import io.cwnr.utils.db.config.PoolParams;
import io.cwnr.utils.db.fn.Binder;
import io.cwnr.utils.db.fn.Mapper;
import io.cwnr.utils.db.fn.PreparedStatementProcessor;
import io.cwnr.utils.db.fn.StatementProcessor;

public class HikariDatabase implements Database {
    private final HikariDataSource dataSource;
    private final int timeout;
    private final Logger logger;

    public HikariDatabase(AuthParams config, PoolParams pool, Duration timeout, Boolean readOnly) {
        this.logger = LoggerFactory.getLogger("HikariDB@" + pool.name());
        HikariConfig hikariConfig = new HikariConfig();
        
        hikariConfig.setReadOnly(readOnly);
        hikariConfig.setAutoCommit(false);
        
        // Consume DBConfig
        hikariConfig.setJdbcUrl(config.jdbcUrl());
        hikariConfig.setUsername(config.username());
        hikariConfig.setPassword(config.password());

        // Consume DBPool
        hikariConfig.setPoolName(pool.name());
        hikariConfig.setMinimumIdle(pool.minPoolSize());
        hikariConfig.setMaximumPoolSize(pool.maxPoolSize());

        // Optimize
        hikariConfig.addDataSourceProperty("cachePrepStmts", "true");
        hikariConfig.addDataSourceProperty("prepStmtCacheSize", "250");
        hikariConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

        // Timeout and stuff
        hikariConfig.setKeepaliveTime(60 * 1000);
        hikariConfig.setLeakDetectionThreshold(20 * timeout.toMillis());
        this.timeout = (int) timeout.toMillis();

        // Metrics and health
        hikariConfig.setMetricRegistry(new MetricRegistry());

        // Pool creation
        this.dataSource = new HikariDataSource(hikariConfig);
        logger.info("Connecting to DB " + dataSource.getPoolName());
        this.dataSource.validate();
    }    

    public String toString() {
        return "HikariDatabase<" + dataSource.getPoolName() + ">: '" + dataSource.getJdbcUrl() + "'";
    }

    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    };

    public Map<String,Integer> getMetrics() {
        HashMap<String,Integer> metrics = new HashMap<String,Integer>();
        MetricRegistry registry = (MetricRegistry) dataSource.getMetricRegistry();
        List.of(
            "ActiveConnections",
            "PendingConnections",
            "IdleConnections",
            "TotalConnections"
        ).stream().forEach((String metricName) -> {
            metrics.put(
                metricName,
                Integer.valueOf(
                    registry.gauge(dataSource.getPoolName() + ".pool." + metricName)
                        .getValue().toString()
                    )
                );
        });
        return metrics;
    }

    public void shutdown() {
        logger.info("Database " + toString() + " shutting down...");
        dataSource.close();
    };


    public <P> P statementExecutor(String sql, StatementProcessor<P> processor) {
        P result = null;
        try (Connection conn = dataSource.getConnection()) {
            conn.setNetworkTimeout(Runnable::run, timeout);
            try (Statement stmt = conn.createStatement()) {
                result = processor.process(stmt);
                conn.commit();
            } catch (SQLException e) {
                logger.error("Error running SQL ({}): {}", e.toString(), sql);
                e.printStackTrace();
                conn.rollback();
            }
        } catch (SQLException e) {
            logger.error("Error handling connection: {}", e.toString());
        }
        return result;
    }

    public <P> P preparedStatementExecutor(String sql, PreparedStatementProcessor<P> processor) {
        P result = null;
        try (Connection conn = dataSource.getConnection()) {
            conn.setNetworkTimeout(Runnable::run, timeout);
            try (PreparedStatement stmt = conn.prepareStatement(sql)) {
                result = processor.process(stmt);
                conn.commit();
            } catch (SQLException e) {
                logger.error("Error running SQL ({}): {}", e.toString(), sql);
                e.printStackTrace();
                conn.rollback();
            }
        } catch (SQLException e) {
            logger.error("Error handling connection: {}", e.toString());
        }
        return result;
    }

    // Execs with no return
    public void exec(String sql) {
        StatementProcessor<Optional<Long>> processor = (Statement stmt) -> {
                stmt.execute(sql);
                return Optional.empty();
            };
        statementExecutor(sql, processor);
    };

    public <I> void exec(String sql, Binder<I> binder, I input) {
        PreparedStatementProcessor<Optional<Long>> processor = (PreparedStatement stmt) -> {
            binder.bind(stmt, input);
            stmt.execute();
            return Optional.empty();
        };
        preparedStatementExecutor(sql, processor);
    }
    
    public <I> void exec(String sql, Binder<I> binder, List<I> inputs) {
        PreparedStatementProcessor<Optional<Long>> processor = (PreparedStatement stmt) -> {
            for (I input : inputs) {
                binder.bind(stmt, input);
                stmt.addBatch();
            }
            stmt.executeLargeBatch();
            return Optional.empty();
        };
        preparedStatementExecutor(sql, processor);
    }

    // Execs with count
    public Optional<Long> execCount(String sql) {
        StatementProcessor<Optional<Long>> processor = (Statement stmt) -> {
            return Optional.of(stmt.executeLargeUpdate(sql));
        };
        return statementExecutor(sql, processor);
    };

    public <I> Optional<Long> execCount(String sql, Binder<I> binder, I input) {
        PreparedStatementProcessor<Optional<Long>> processor = (PreparedStatement stmt) -> {
            binder.bind(stmt, input);
            return Optional.of(stmt.executeLargeUpdate());
        };
        return preparedStatementExecutor(sql, processor);
    }

    public <I> Optional<Long> execCount(String sql, Binder<I> binder, List<I> inputs) {
        PreparedStatementProcessor<Optional<Long>> processor = (PreparedStatement stmt) -> {
            for (I input : inputs) {
                binder.bind(stmt, input);
                stmt.addBatch();
            }
            long updatedRows = stmt.executeLargeBatch().length;
            return Optional.of(updatedRows);
        };
        return preparedStatementExecutor(sql, processor);
    }

    // Select statement
    public <O> List<O> select(String sql, Mapper<O> mapper) {
        StatementProcessor<List<O>> processor = (Statement stmt) -> {
            List<O> result = new LinkedList<O>();
            stmt.execute(sql);
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) result.add(mapper.map(rs));
            return result;
        };
        return statementExecutor(sql, processor);
    };

    public <O> List<O> select(String sql, Mapper<O> mapper, long count) {
        StatementProcessor<List<O>> processor = (Statement stmt) -> {
            List<O> result = new LinkedList<O>();
            long i = 0;
            stmt.execute(sql);
            ResultSet rs = stmt.getResultSet();
            while(rs.next() && i++ < count) result.add(mapper.map(rs));
            return result;
        };
        return statementExecutor(sql, processor);
    };

    // Select prepared statement
    public <I,O> List<O> select(String sql, Binder<I> binder, Mapper<O> mapper, I input) {
        PreparedStatementProcessor<List<O>> processor = (PreparedStatement stmt) -> {
            List<O> result = new LinkedList<O>();
            binder.bind(stmt, input);
            stmt.execute();
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) result.add(mapper.map(rs));
            return result;
        };
        return preparedStatementExecutor(sql, processor);
    };

    public <I,O> List<O> select(String sql, Binder<I> binder, Mapper<O> mapper, List<I> inputs) {
        PreparedStatementProcessor<List<O>> processor = (PreparedStatement stmt) -> {
            List<O> result = new LinkedList<O>();
            for (I input : inputs) {
                binder.bind(stmt, input);
                stmt.addBatch();
            }
            stmt.executeLargeBatch();
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) result.add(mapper.map(rs));
            return result;
        };
        return preparedStatementExecutor(sql, processor);
    };

    // Select string -> long map
    public HashMap<String,Long> selectStringLongMap(String sql) {
        StatementProcessor<HashMap<String, Long>> processor = (Statement stmt) -> {
            HashMap<String, Long> result = new HashMap<String, Long>();
            stmt.execute(sql);
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) result.put(rs.getString(1), rs.getLong(2));
            return result;
        };

        return statementExecutor(sql, processor);
    };
    
    public <I> HashMap<String,Long> selectStringLongMap(String sql, Binder<I> binder, I input) {
        PreparedStatementProcessor<HashMap<String,Long>> processor = (PreparedStatement stmt) -> {
            HashMap<String, Long> result = new HashMap<String, Long>();
            binder.bind(stmt, input);
            stmt.executeBatch();
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) {
                result.put(rs.getString(1), rs.getLong(2));
            }
            return result;
        };
        return preparedStatementExecutor(sql, processor);
    };

    // Select string -> string map
    public HashMap<String,String> selectStringStringMap(String sql) {
        StatementProcessor<HashMap<String, String>> processor = (Statement stmt) -> {
            HashMap<String, String> result = new HashMap<String, String>();
            stmt.execute(sql);
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) result.put(rs.getString(1), rs.getString(2));
            return result;
        };

        return statementExecutor(sql, processor);
    };
    
    public <I> HashMap<String,String> selectStringStringMap(String sql, Binder<I> binder, I input) {
        PreparedStatementProcessor<HashMap<String,String>> processor = (PreparedStatement stmt) -> {
            HashMap<String, String> result = new HashMap<String, String>();
            binder.bind(stmt, input);
            stmt.executeBatch();
            ResultSet rs = stmt.getResultSet();
            while(rs.next()) {
                result.put(rs.getString(1), rs.getString(2));
            }
            return result;
        };
        return preparedStatementExecutor(sql, processor);
    }
}
