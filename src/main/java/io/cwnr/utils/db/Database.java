package io.cwnr.utils.db;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import io.cwnr.utils.db.fn.Binder;
import io.cwnr.utils.db.fn.Mapper;

public interface Database {
    public Connection getConnection() throws SQLException;
    public Map<String,Integer> getMetrics();
    public void shutdown();

    // Diag methods
    public String toString();

    // Execs with no return
    public void exec(String sql);
    public <I> void exec(String sql, Binder<I> binder, I input);
    public <I> void exec(String sql, Binder<I> binder, List<I> inputs);

    // Execs with count
    public Optional<Long> execCount(String sql);
    public <I> Optional<Long> execCount(String sql, Binder<I> binder, I input);
    public <I> Optional<Long> execCount(String sql, Binder<I> binder, List<I> inputs);

    // Select statement
    public <O> List<O> select(String sql, Mapper<O> mapper);
    public <O> List<O> select(String sql, Mapper<O> mapper, long count);

    // Select prepared statement
    public <I,O> List<O> select(String sql, Binder<I> binder, Mapper<O> mapper, I input);
    public <I,O> List<O> select(String sql, Binder<I> binder, Mapper<O> mapper, List<I> inputs);
    
    // Select string -> long map
    public HashMap<String,Long> selectStringLongMap(String sql);
    public <I> HashMap<String,Long> selectStringLongMap(String sql, Binder<I> binder, I inputs);

    // Select string -> string map
    public HashMap<String,String> selectStringStringMap(String sql);
    public <I> HashMap<String,String> selectStringStringMap(String sql, Binder<I> binder, I inputs);   
}
