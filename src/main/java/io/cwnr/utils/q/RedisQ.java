package io.cwnr.utils.q;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisQ<E> implements Q<E> {

    private final JedisPool jedisPool;
    private final String queueName;
    private final Class<E> elementClass;
    private final ObjectMapper objectMapper;
    private final Logger logger;

    public RedisQ(Class<E> elementClass, JedisPool jedisPool, String queueName) {
        this.elementClass = elementClass;
        this.jedisPool = jedisPool;
        this.queueName = queueName;
        this.objectMapper = new ObjectMapper().setSerializationInclusion(Include.NON_NULL);
        this.logger = LoggerFactory.getLogger("RedisQ@" + queueName);
    }

    public String toString() {
        return "RedisQ<" + elementClass.getName() + "@" + queueName + "> [" + size() + " elements]";
    };

    public long size() {
        try (Jedis redis = jedisPool.getResource()) {
            return size(redis);
        }
    };

    public long size(Jedis redis) {
        return redis.llen(queueName);
    }

    public boolean isEmpty() {
        return size() == 0;
    };

    public void clear() {
        try (Jedis redis = jedisPool.getResource()) {
            redis.del(queueName);
        }
    };

    public Optional<E> get() {
        List<E> elements = get(1);
        if (elements.size() < 1) return Optional.empty();
        return Optional.of(elements.get(0));
    };

    public List<E> get(long elements) {
        List<E> result = new LinkedList<E>();
        try (Jedis redis = jedisPool.getResource()) {
            List<String> serializedResult = redis.lrange(queueName, 0L, elements - 1);
            if (serializedResult != null && !serializedResult.isEmpty()) {
                for (String serializedElement : serializedResult) {
                    try {
                        result.add(objectMapper.readValue(serializedElement, elementClass));
                    } catch (JsonMappingException e){
                        logger.error("Can't map string '" + serializedElement + "' to " + elementClass.getName());
                    } catch (JsonProcessingException e) {
                        logger.error("Can't process string '" + serializedElement);
                    }
                }
            }
        }
        return result;
    };

    public List<E> getAll() {
        return get(size());
    };

    public Optional<E> poll() {
        List<E> elements = poll(1);
        if (elements.size() < 1) return Optional.empty();
        return Optional.of(elements.get(0));
    };

    public List<E> poll(long elements) {
        if (elements > Integer.MAX_VALUE) {
            throw new IllegalStateException("Can't pool more than " + Integer.MAX_VALUE + " elements");
        }
        List<E> result = new LinkedList<E>();
        try (Jedis redis = jedisPool.getResource()) {
            List<String> serializedResult = redis.lpop(queueName, (int) elements);
            if (serializedResult != null && !serializedResult.isEmpty()) {
                for (String serializedElement : serializedResult) {
                    try {
                        result.add(objectMapper.readValue(serializedElement, elementClass));
                    } catch (JsonMappingException e){
                        logger.error("Can't map string '" + serializedElement + "' to " + elementClass.getName());
                    } catch (JsonProcessingException e) {
                        logger.error("Can't process string '" + serializedElement);
                    }
                }
            }
        }
        return result;
    };

    public List<E> pollAll() {
        return poll(size());
    };

    public long add(E element) {
        List<E> elements = new LinkedList<E>();
        elements.add(element);
        return addAll(elements);
    };

    public long addAll(List<E> elements) {
        // If nothing to add we return current size
        if (elements == null || elements.isEmpty()) return size();

        List<String> serializedElements = new LinkedList<String>();
        try (Jedis redis = jedisPool.getResource()) {
            // Serialize elements to Strings
            for (E element : elements) {
                try {
                    serializedElements.add(objectMapper.writeValueAsString(element));
                }  catch (JsonMappingException e){
                    logger.error("Can't serialize object '" + element + "' of " + elementClass.getName());
                } catch (JsonProcessingException e) {
                    logger.error("Can't process object '" + element);
                }
            }
            String[] elementToPush = serializedElements.toArray(new String[serializedElements.size()]); 
            redis.rpush(queueName, elementToPush);
            return size(redis);
        }
    };
}
