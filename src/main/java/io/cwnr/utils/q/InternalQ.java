package io.cwnr.utils.q;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentLinkedQueue;

public class InternalQ<E> implements Q<E> {
    private final ConcurrentLinkedQueue<E> queue;
    private final Class<E> elementClass;

    public InternalQ(Class<E> elementClass) {
        queue = new ConcurrentLinkedQueue<E>();
        this.elementClass = elementClass;
    }

    public String toString() {
        return "InternalQ<" + elementClass.getName() + "> [" + size() + " elements]";
    }

    public long size() {
        return queue.size();
    };

    public boolean isEmpty() {
        return queue.isEmpty();
    };

    public void clear() {
        queue.clear();
    };

    public Optional<E> get() {
        return Optional.ofNullable(this.queue.peek());
    };

    public List<E> get(long elements) {
        List<E> result = new LinkedList<E>();
        Iterator<E> iterator = queue.iterator();
        long elementsCounter = 0L;
        while (elementsCounter < elements && iterator.hasNext()) {
            elementsCounter++;
            result.add(iterator.next());
        }
        return result;
    };

    public List<E> getAll() {
        return new LinkedList<E>(this.queue);
    }

    public Optional<E> poll() {
        return Optional.ofNullable(queue.poll());
    };

    public List<E> poll(long elements) {
        List<E> result = new LinkedList<E>();
        for (long i = 0L; i < elements; i++) {
            E element = queue.poll();
            if (element == null) break;
            result.add(element);
        }
        return result;
    };

    public List<E> pollAll() {
        List<E> result = new LinkedList<E>();
        while (true) {
            E element = queue.poll();
            if (element == null) break;
            result.add(element);
        }
        return result;
    };

    public long add(E element) {
        if (element != null) queue.add(element);
        return this.size();
    };

    public long addAll(List<E> elements) {
        for (E element : elements) queue.add(element);
        return this.size();
    };
}
