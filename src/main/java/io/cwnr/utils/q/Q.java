package io.cwnr.utils.q;

import java.util.List;
import java.util.Optional;

public interface Q<E> {
    public String toString();
    public long size();
    public boolean isEmpty();
    public void clear();

    public Optional<E> get();
    public List<E> get(long elements);
    public List<E> getAll();

    public Optional<E> poll();
    public List<E> poll(long elements);
    public List<E> pollAll();

    public long add(E element);
    public long addAll(List<E> elements);
}
